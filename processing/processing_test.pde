void setup() {
    if (window.canvasWidth != null && window.canvasHeight != null) {
        size(window.canvasWidth, window.canvasHeight);
    } else {
        size(1000, 500);
    }
    background(0);
}

float angle = 0;
void draw() {
    if (resized()) {
        resizeCanvas()
    }

    angle = (angle + random(2)) % 360;
    stroke(random(50), random(255), random(255), 32);
    strokeWeight(10);

    drawLine(angle);
    drawLine(angle+90);
    drawLine(angle+180);
    drawLine(angle+270);
}

void drawLine(float angle) {
    angle = angle % 360;
    float centerX = width/2;
    float centerY = height/2;
    float radius = min(width, height);

    float x1 = centerX + (radius/4) * cos(PI*angle/180);
    float y1 = centerY + (radius/4) * sin(PI*angle/180);
    float x2 = centerX + (radius/2) * cos(PI*angle/180);
    float y2 = centerY + (radius/2) * sin(PI*angle/180);
    line(x1, y1, x2, y2);
}

boolean resized() {
    boolean variableSetted = window.canvasWidth != null && window.canvasHeight != null;
    boolean resized = variableSetted && (window.canvasWidth != width || window.canvasHeight != height);
    return resized;
}

void resizeCanvas() {
    console.log(window.canvasWidth, window.canvasHeight);
    size(window.canvasWidth, window.canvasHeight);
}