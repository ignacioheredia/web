// ignacio.nh at gmail

window.onload = function main() {
	var backCanvas = document.getElementById("backCanvas");
	var midCanvas = document.getElementById("midCanvas");
	var fronCanvas = document.getElementById("frontCanvas");

	var width, heigth;
	function resizeCanvas() {
		width = document.documentElement.clientWidth;
		height = document.documentElement.clientHeight;

		backCanvas.width = width;
		backCanvas.height = height;
		midCanvas.width = width;
		midCanvas.height = height;
		frontCanvas.width = width;
		frontCanvas.height = height;
	}

	resizeCanvas();
	backCtx = backCanvas.getContext("2d");
	midCtx = midCanvas.getContext("2d");
	frontCtx = frontCanvas.getContext("2d");

	var FPS = 30;
	var handle = setInterval(animation, 1000/FPS);

	var currentFrame = 0;
	dinosaur = new TRex;
	walkingFloor = new Floor;

	function animation() {
		currentFrame += 1;
		currentFrame %= 30;
		if (currentFrame%5==0) {
			dinosaur.graph();
			dinosaur.move();
		}
		walkingFloor.changeColors();
		walkingFloor.graph();
		walkingFloor.move();
	}

	document.addEventListener("click", function(e){
		var x = e.clientX;
		var y = e.clientY;
		if (dinosaur.mouseover(x,y)) {
			dinosaur.toggleMoving();
		}
		else if (walkingFloor.squareIn(x,y)) {
		    walkingFloor.changeSquareColor(x,y);
		}
	});

	window.onresize = function () {
		resizeCanvas();
		dinosaur.resize();
		walkingFloor.resize();
		backCtx.clearRect(0,0,width,height);
		midCtx.clearRect(0,0,width,height);
	}

	function TRex() {
		this.width = 76;
		this.height = 86;
		this.x = Math.round(width/2 - this.width/2);
		this.y = Math.round(height/2 - this.height/2);
		this.speed_x=10;

		this.state = "walk";
		this.currentSprite = 0;
		this.walkImg = new Array;
		this.idleImg = new Array;

        	this.savedSpritesAmount = 20;
		this.lastSprites = new Array;

		for (var i=0; i<6; i++) {
			this.walkImg[i] = document.getElementById("walk"+i);
			this.idleImg[i] = document.getElementById("idle"+i);

		}

		this.initSprites = function() {
			for (var i=0; i<this.savedSpritesAmount; i++) {
				this.lastSprites[i] = {
					img:document.getElementById("idle0"),
					x:this.x,
					y:this.y
				}
			}
		}
		this.initSprites();

		this.resize = function() {
			for (var i=0; i<this.savedSpritesAmount; i++) {
				this.lastSprites[i].diffX = this.lastSprites[i].x - this.x;
				this.lastSprites[i].diffY = this.lastSprites[i].y - this.y;
			}

			this.x = Math.round(width/2 - this.width/2);
			this.y = Math.round(height/2 - this.height/2);

			for (var i=0; i<this.savedSpritesAmount; i++) {
				this.lastSprites[i].x = this.lastSprites[i].diffX + this.x;
				this.lastSprites[i].y = this.lastSprites[i].diffY + this.y;
			}
		};

		this.graph = function() {
			var img, x, y;
			if (this.state=="idle") { 
				img=this.idleImg[this.currentSprite]; 
				x=this.x; 
				y=this.y
			}
			else { 
				img=this.walkImg[this.currentSprite]; 
				x=this.x+6;
				y=this.y+6;
			}

			for (var i=0; i<this.savedSpritesAmount; i++) {
				this.lastSprites[i] = this.lastSprites[i+1];
			}
			this.lastSprites[this.savedSpritesAmount-1] = {
				img:img,
				x:x,
				y:y
			};

			midCtx.clearRect(0,this.y,width,100);

			for (var i=0; i<this.savedSpritesAmount-1; i++) {
				midCtx.globalAlpha = (i+1)/this.savedSpritesAmount;
				midCtx.drawImage(this.lastSprites[i].img, this.lastSprites[i].x, this.lastSprites[i].y);
			}

			midCtx.globalAlpha = 1;
			var i = this.savedSpritesAmount-1;
			midCtx.drawImage(this.lastSprites[i].img, this.lastSprites[i].x, this.lastSprites[i].y);
            		this.currentSprite += 1;
			this.currentSprite %= 6;
		};

		this.move = function() {
			if (this.state == "walk") {
				for (var i=0; i<this.savedSpritesAmount; i++) {
					this.lastSprites[i].x -= this.speed_x;
				}
			}
		};

		this.mouseover = function(x,y) {
			return (this.x <= x && x <= this.x+this.width && this.y <= y && y <= this.y+this.height);
		};

		this.toggleMoving = function() {
			if (this.state == "idle") { this.state = "walk"; }
			else { this.state = "idle"; }
		};

	}

	function Floor() {
		this.height = 50;
		this.backGroundColor = "#CCCCCC";
		this.speed = 2;
		this.rectSize = 40;
		this.rects = new Array;
		
		this.resize = function () {
			this.y = Math.round(height/2)+20;
			this.rectAmount = Math.ceil(width/this.rectSize)+1;
			for (var i=0; i<this.rectAmount; i++) {
				this.rects[i] = {
					x:i*this.rectSize,
					y:equiprobable(0, this.height-this.rectSize),
					color:randomVectorColor(0,3)

				}
			}
		};
		this.resize();

		this.graph = function() {
			backCtx.clearRect(0,this.y,width,this.height);
			for (var i=0; i<this.rectAmount; i++) {
				backCtx.fillStyle = colorize(this.rects[i].color);
				backCtx.fillRect(this.rects[i].x, this.y+this.rects[i].y, this.rectSize, this.rectSize);
			}

		};

		this.move = function() {
			if (dinosaur.state == "walk") {
				for (var i=0; i<this.rectAmount; i++) {
					this.rects[i].x -= this.speed;
					if (this.rects[i].x <= -this.rectSize) {
						this.rects[i].x = (this.rectAmount-1)*this.rectSize;
						this.rects[i].color = randomVectorColor(0,3);
					}
				}
			}
		};

		this.changeColors = function() {
			var i = equiprobable(0,this.rectAmount-1);
			var j = equiprobable(0,6);
			if (this.rects[i].color[j]==0) { this.rects[i].color[j]=1; }
			else if (this.rects[i].color[j]==15) { this.rects[i].color[j]=14; }
			else { this.rects[i].color[j] += equiprobable(-1,1); }
		};

		this.squareIn = function(x,y) {
		    for (var i=0; i<this.rectAmount; i++) {
		        var rectX = this.rects[i].x;
		        var rectY = this.y+this.rects[i].y;
		        if (rectX <= x && x <=rectX+this.rectSize && rectY <= y && y <= rectY+this.rectSize) {
		            return true;
		        }
		    }
		    return false;
		};

		this.changeSquareColor = function(x,y) {
		    for (var i=0; i<this.rectAmount; i++) {
		        var rectX = this.rects[i].x;
		        var rectY = this.y+this.rects[i].y;
		        if (rectX <= x && x <=rectX+this.rectSize && rectY <= y && y <= rectY+this.rectSize) {
		            this.rects[i].color = randomVectorColor(0,4);
		            return true;
		        }
		    }
		    return false;
		};
	}
};


function cosine(a) {
	// a en grados
	return Math.cos(a*Math.PI/180);
}

function sine(a) {
	// a en grados
	return Math.sin(a*Math.PI/180);
}

function uniform(a,b) {
	return Math.random()*(b-a) + a;
}

function equiprobable(a,b) {
	return Math.floor(Math.random()*(1+b-a) + a);
}

function randomVectorColor(a,b) {
	var exit = [];
	for (var i=0; i<6; i++) { exit[i] = equiprobable(a,b); }
	return exit;
}

function colorize(vectorColor) {
	var exit = "#";
	for (var i=0; i<6; i++) { exit+=vectorColor[i].toString(16); }
	return exit;
}
