var width, height, distanciaMax;
var FPS=30;
var canvas = document.getElementById('canvas');
var figuras, separacion, colorPrincipal;
var lado = 0;

function resize() {
	if (typeof handle!= "undefined") {
		window.clearInterval(handle);
	}
	width = document.documentElement.clientWidth;
	height = document.documentElement.clientHeight;
	document.getElementById("canvas").width = width;
	document.getElementById("canvas").height = height;
	distanciaMax = Math.sqrt( (width/2)*(width/2) + (height/2)*(height/2) ) * 0.75; 

	ctx = canvas.getContext("2d");

	colorPrincipal = [equiprobable(7,16)*16,equiprobable(7,16)*16,equiprobable(7,16)*16];
	separacion = equiprobable(40,100);
	lado = separacion -5;
	figuras = new Array;
	var centroX = width/2;
	var centroY = height/2;
	for (i=0; i<width/(2*separacion)+1; i++) {
		for (j=0; j<height/(2*separacion)+1; j++) {
			var c = new Cuadrado();
			c.color = color(colorPrincipal);
			c.x = centroX  - i*separacion;
			c.y = centroY + j*separacion;
			figuras.push(c);

			var c = new Cuadrado();
			c.color = color(colorPrincipal);
			c.x = centroX + i*separacion;
			c.y = centroY - j*separacion;
			figuras.push(c);

			var c = new Cuadrado();
			c.color = color(colorPrincipal);
			c.x = centroX - i*separacion;
			c.y = centroY - j*separacion;
			figuras.push(c);

			var c = new Cuadrado();
			c.color = color(colorPrincipal);
			c.x = centroX + i*separacion;
			c.y = centroY + j*separacion;
			figuras.push(c);
		}
	}

	colorPrincipal = [equiprobable(7,16)*16,equiprobable(7,16)*16,equiprobable(7,16)*16];
	handle = setInterval(animacion,1000/FPS);

}

function animacion() {
	ctx.clearRect(0,0,width,height)
	for (i=0; i<figuras.length; i++) {
		figuras[i].graficar();
	}
}

function mouseMove(event) {
	var x = event.clientX;
	var y = event.clientY;
	var dis = distancia(x,y);
	if (dis>separacion && dis<distanciaMax) {
		lado = conversionLineal(dis,0,distanciaMax,0,separacion-5)
	}
	else if (dis<=separacion){
		window.clearInterval(handle);
		resize();
		lado=0;
	}
	else {
		lado = separacion-5;
	}
}

function mouseClick(event) {
	var x = event.clientX;
	var y = event.clientY;
	for (i=0; i<figuras.length; i++) {
		if (Math.abs(figuras[i].x-x)<separacion*2 && Math.abs(figuras[i].y-y)<separacion*2) {
			figuras[i].color = color(colorPrincipal);
		}
	}
}

function distancia(a,b) {
	var x = width/2-a;
	var y = height/2-b;
	return Math.sqrt(x*x+y*y);
}

function color(array) {
	var offset=equiprobable(-70,70);
	var exit = [array[0]+offset,array[1]+offset,array[2]+offset];
	offset=equiprobable(-20,20);
	exit[equiprobable(0,2)]+=offset;
	return exit;
}

function conversionLineal(t,x0,y0,x1,y1) {
	// funcion que evalua t en la funcion lineal que manda el intervalo [x0,y0] al [x1,y1]
	var m = (x1-y1)/(x0-y0);
	var b = x1 - m*x0;
	return m*t+b;
}

function Cuadrado() {
	this.x;
	this.y;
	this.color;
	this.graficar = function () {
		ctx.fillStyle = "rgb("+this.color[0]+","+this.color[1]+","+this.color[2]+")";
		ctx.fillRect(this.x-lado/2,this.y-lado/2,lado,lado);
	}
}

function uniforme(a,b) {
	return Math.random()*(b-a) + a;
}

function equiprobable(a,b) {
	return Math.floor(Math.random()*(1+b-a) + a);
}
