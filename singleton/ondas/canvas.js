$(function() {
	var canvas = $("#canvas");

	var decimalLength = 50; // in px;
	drawAxis(canvas[0], decimalLength);

	$("#eval").on('click', function() {
		var functionString = $("#function").val();
		drawFunction(functionString, decimalLength);
	});
});

function drawAxis(canvas, decimalLength) {
	// Hardcoded ugly stuff

	var ctx = canvas.getContext("2d");

	var xAxisWidth = 67; // 2 (space) + 63 after 0 (just passing 2*PI) + 2 (space)
	var yAxisHeight = 24; // 2 (space) + 10 above 0 + 10 below 0 + 2 (space) 

	var width = xAxisWidth*decimalLength;
	var height = yAxisHeight*decimalLength;
	canvas.width = width;
	canvas.height = height;

	ctx.strokeStyle = "#000";
	ctx.lineWidth = 4;

	// --- Y-axis ---
	ctx.moveTo(2*decimalLength, decimalLength);
	ctx.lineTo(2*decimalLength, (yAxisHeight-1)*decimalLength);
	ctx.stroke();

	ctx.lineWidth = 2;
	for (var i=1; i<=10; i++) {
		var yOffset = decimalLength*i;
		
		ctx.moveTo(1.75*decimalLength, (yAxisHeight/2)*decimalLength+yOffset);
		ctx.lineTo(2.25*decimalLength, (yAxisHeight/2)*decimalLength+yOffset);
		ctx.stroke();

		ctx.moveTo(1.75*decimalLength, (yAxisHeight/2)*decimalLength-yOffset);
		ctx.lineTo(2.25*decimalLength, (yAxisHeight/2)*decimalLength-yOffset);
		ctx.stroke();
	}
	
	ctx.fillStyle = "#000";
	ctx.textBaseline="middle"; 
	ctx.font='30px Arial';
	ctx.textAlign="end"; 
	ctx.fillText("1", decimalLength*1.5, decimalLength*2);
	ctx.fillText("-1", decimalLength*1.5, decimalLength*22);

	// --- X-axis ---
	ctx.lineWidth = 4;
	ctx.moveTo(decimalLength, (yAxisHeight/2)*decimalLength);
	ctx.lineTo((xAxisWidth-1)*decimalLength, (yAxisHeight/2)*decimalLength);
	ctx.stroke();

	ctx.lineWidth = 2;
	for (var i=1; i<=63; i++) {
		ctx.moveTo(decimalLength*i+2*decimalLength, (yAxisHeight/2)*decimalLength-0.25*decimalLength);
		ctx.lineTo(decimalLength*i+2*decimalLength, (yAxisHeight/2)*decimalLength+0.25*decimalLength);
		ctx.stroke();
	}

	ctx.fillStyle = "#000";
	ctx.textBaseline="top"; 
	ctx.font='30px Arial';
	ctx.textAlign="center"; 
	ctx.fillText("1", decimalLength*12, decimalLength*11);
	ctx.fillText("2", decimalLength*22, decimalLength*11);
	ctx.fillText("3", decimalLength*32, decimalLength*11);
	ctx.fillText("4", decimalLength*42, decimalLength*11);
	ctx.fillText("5", decimalLength*52, decimalLength*11);
	ctx.fillText("6", decimalLength*62, decimalLength*11);
	ctx.fillText("pi", decimalLength*2 + decimalLength * Math.PI * 10, decimalLength*11);
	ctx.fillText("2pi", decimalLength*2 + decimalLength * Math.PI * 20, decimalLength*11);
}

function drawFunction(functionString, decimalLength) {
	var canvas = $("#canvas");
	var ctx = canvas[0].getContext("2d");

	ctx.clearRect(0, 0, canvas[0].width, canvas[0].height);
	drawAxis(canvas[0], decimalLength);

	ctx.strokeStyle = "#E22";
	ctx.lineWidth = 4;

	var yCenter = decimalLength*12;
	var f = new Function("x",functionString);
	ctx.beginPath();
	var x = 2*decimalLength;
	var y = f(0);
	ctx.moveTo(x, yCenter-y*decimalLength*10);
	for (var i=2*decimalLength+1; i<65*decimalLength; i++) {
		var x = (i-2*decimalLength)/(decimalLength*10);
		var y = f(x);
		y = Math.max(y,-1);
		y = Math.min(y,1);
		ctx.lineTo(i, yCenter-y*decimalLength*10);
	}
	ctx.stroke();
}
