function Note(params) {
	params = params || {};

	this.nativeAudio = new Audio();
	this.wave = new RIFFWAVE();
	this.data = [];

	this.sampleRate = params.sampleRate || 44100;
	this.wave.header.sampleRate = this.sampleRate;

	this.duration = params.duration || 1000; // in milliseconds
	this.sampleCount = this.sampleRate * this.duration / 1000;

	this.volume = params.volume || 128;
	this.frequency = params.frequency || 440;
	var f = params.oscillationFunction || Math.sin;
	for (var i=0; i<this.sampleCount; i++) {
		var evaluation = f( 2*Math.PI*this.frequency*i/this.sampleRate );
		evaluation = Math.min(evaluation, 1);
		evaluation = Math.max(evaluation, -1);
		var oscillation = Math.round((this.volume-1) * evaluation);
		this.data[i++] = this.volume + oscillation;
	}

	this.wave.Make(this.data);
	this.nativeAudio.src = this.wave.dataURI;

	this.play = function() {
		this.nativeAudio.play();
	}

}

$(function() {
	$("#sound").on('click', function() {

		var functionString = $("#function").val();
		var f = new Function("x",functionString);

		(new Note({
			oscillationFunction: f
		})).play();
	});
});
