function Disc(params) {
    this.angle = params.initial_angle;
    this.radius = params.initial_radius;
    this.color = new Color({min:192});

    this.move = function() {
        this.angle += params.direction * params.angle_speed;
        this.angle %= 360;

        this.radius += params.radius_speed;
        this.radius %= params.max_radius;
    };

    this.paint = function() {
        params.ctx.fillStyle = this.color.toString();

        var x = params.centerX + this.radius*cosine(this.angle) + randomVariable.Equiprobable(-1,1) * params.shake;
        var y = params.centerY + this.radius*sine(this.angle) + randomVariable.Equiprobable(-1,1) * params.shake;

        params.ctx.beginPath();
        params.ctx.arc(x, y , 10, 0, 2*Math.PI);
        //params.ctx.stroke();
        params.ctx.fill();
    };

    this.update_color = function() {
        this.color.mutate(8)
    }
}

$(function() {

    var canvas = $("#canvas");
    var canvas_properties = setUpCanvas("#canvas");

	var discs;
	function init() {
		discs = [];

		var number_of_discs = randomVariable.Equiprobable(50,100);
		var rounds = randomVariable.Equiprobable(1,10);
		var spread = randomVariable.Equiprobable(1,10)/10;
		var angle_speed = randomVariable.Uniform(0,1);
		var radius_speed = randomVariable.Uniform(1,3);
		var shake = randomVariable.Uniform(0.5,2.5);
    	var diagonal =  canvas_properties.diagonal/2;

		for (var i=0; i<number_of_discs; i++) {
			var direction;
			if (randomVariable.Equiprobable(0,1) == 1) { direction = 1; }
			else { direction = -1; }
			discs[i] = new Disc({
                direction: direction,
                initial_angle: 360*i*rounds/(number_of_discs),
                initial_radius: i*diagonal*spread/number_of_discs,
                angle_speed: angle_speed,
                radius_speed: radius_speed,
                shake: shake,
                max_radius: diagonal,
                ctx: canvas_properties.ctx,
                centerX: canvas_properties.width/2,
                centerY: canvas_properties.height/2
            });
		}
	}
    init();

    canvas.click(init);
    $(window).resize(function() {
        canvas_properties = setUpCanvas("#canvas");
        init();
    });

	var FPS = 30;
	setInterval(function() {
		for (var i=0; i<discs.length; i++) {
			discs[i].move();
			discs[i].paint();
			discs[i].update_color();
		}
    }, 1000/FPS);

});
