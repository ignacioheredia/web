$(function() {
	$("#searchInput").keyup(function(event){
		if(event.keyCode == 13){
			query = $("#searchInput").val();
			if (isUrl(query)) {
				loadUrl(query);
			}
			else {
			    search();
			}
		}
	});

	$("#go").on('click', function() {
		query = $("#searchInput").val();
		if (isUrl(query)) {
			loadUrl(query);
		}
		else {
		    search();
		}
	});

	$("#goBack").on('click', goBack);

	checkHash();
});

function checkHash() {
	var query = document.location.hash.substr(1);

	if (isUrl(query)) {
		loadUrl(query);
	}
	else {
		query = query.split(/[ ,\+\.\\\/\?-]+/g).join(' ');
		if (query) {
			$("#searchInput").val(query);
			search();
		}
		else {
			$("#search").show();
		}
	}
}

function isUrl(query) {
	return /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/.test(query);

}

function updateHash(query) {
	if (isUrl(query)) {
		document.location.hash = query;
	}
	else {
		document.location.hash = query.replace(/[ ,\+\.\\\/\?-]+/g, '+');
	}
}

function search() {
	$("#search").removeClass('error').fadeOut();
	$(".loader").fadeIn();

	query = $("#searchInput").val() || 'psychedelic';
	updateHash(query);

	var imageSearch = new google.search.ImageSearch();
	imageSearch.setSearchCompleteCallback(imageSearch, searchComplete, null);
	imageSearch.setResultSetSize(8);
	imageSearch.setRestriction(
		google.search.ImageSearch.RESTRICT_IMAGESIZE,
		google.search.ImageSearch.IMAGESIZE_LARGE
	);
	imageSearch.execute(query);
}

function searchComplete() {
	var imageSearch = this;
	if (imageSearch.results && imageSearch.results.length > 0) {
		window.displayKaleid = true;
		displayImage(imageSearch, 0);
		$("#goBack").fadeIn();
	}
	else {
		$(".loader").fadeOut();
		$("#search").addClass('error').fadeIn();
	}
}

function displayImage(imageSearch, currentImage) {

	if (currentImage >= imageSearch.results.length) {
		currentImage = 0;
		var currentPage = imageSearch.cursor.currentPageIndex;
		currentPage += 1;
		currentPage %= imageSearch.cursor.pages.length;
		imageSearch.gotoPage(currentPage);
	}
	else {
		var result = imageSearch.results[currentImage];
		var imageDuration = 8000;
		var newImg = $("<img>");
		newImg.on('load', function() {
			kaleid(newImg);
			$(".loader").hide();
			currentImage++;
			window.timer = setTimeout(function() {
				if (window.displayKaleid) {
					displayImage(imageSearch, currentImage)
				}
			}, imageDuration);
		});
		newImg.error(function() {
			currentImage++;
			displayImage(imageSearch, currentImage)
		});
		newImg.attr('src',result.url);
	}
}

function loadUrl(url) {
	updateHash(url);
	$("#search").hide();
	$(".loader").show();
	var newImg = $("<img>");
	newImg.on('load', function() {
		kaleid(newImg);
		$(".loader").fadeOut();
		$("#goBack").show();
	});
	newImg.error(function() {
		$("#search").addClass("error");
		$("#searchInput").val("error");
		goBack();
	});
	newImg.attr('src', url);
}

function kaleid(img) {
	img.hide();
	switchImage(img, 'original');
	switchImage(img, 'flipH');
	switchImage(img, 'flipV');
	switchImage(img, 'flipHV');

}

function switchImage(img, identifier) {
	var container = $("#" + identifier);
	var transitionDuration = 6000;

	var toRemove = container.children();
	toRemove.fadeOut({
		duration: transitionDuration,
		complete: function() {
			toRemove.remove();
		}
	});

	var noAnimation = img.clone().addClass(identifier + ' noAnimation');
	container.append(noAnimation);
	noAnimation.fadeIn(transitionDuration);

	var original = img.clone().addClass(identifier);
	container.append(original);
	original.fadeIn(transitionDuration);
}

function goBack() {
	$(".loader").hide();
	updateHash('');
	clearTimeout(window.timer);
	window.displayKaleid = false;
	$("#search").fadeIn();
	$("#goBack").fadeOut();
}
