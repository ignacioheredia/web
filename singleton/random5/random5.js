$(function() {

    // Canvas stuff:
	var canvas = $("#canvas");
    var canvas_properties = setUpCanvas("#canvas", {
        globalAlpha: 0.2,
        lineWidth: 16
    });

    // Settings:
    var settings = {
        draw: true,
        mutateColor: true,
        mutationSpeed: 16,
        spread: 0.05

    };

    // Main class:
	var circle = {
        color: new Color(),
        angle: 0,
        mouseDistance: -1,

        draw: function() {
            var mouse = 0.5;
            if (this.mouseDistance != -1) { mouse = this.mouseDistance; }

            var spread = randomVariable.Uniform(1-settings.spread, 1+settings.spread);
            var radius = (canvas_properties.diagonal/4) * mouse * spread;
            var x = canvas_properties.width/2 + Math.cos(this.angle)*radius;
            var y = canvas_properties.height/2 + Math.sin(this.angle)*radius;

            canvas_properties.ctx.beginPath();
            canvas_properties.ctx.arc(x, y, radius, 0, 2*Math.PI);
            canvas_properties.ctx.stroke();

            this.angle = randomVariable.Uniform(0, 2*Math.PI);
        }
	};


    var FPS = 30;
	setInterval(function() {
        if (settings.draw) {
            for (var i=0; i<20; i++) {
                canvas_properties.ctx.strokeStyle = circle.color.toString();
                circle.draw();
            }
            if (settings.mutateColor)
                circle.color.mutate(settings.mutationSpeed);
        }

	}, 1000/FPS);

    canvas.mousemove(function(e) {
        var x = e.pageX - canvas.offset().left - canvas_properties.width/2;
        var y = e.pageY - canvas.offset().top - canvas_properties.height/2;
        circle.mouseDistance = Math.sqrt(x*x + y*y) / (canvas_properties.diagonal/2);
    });

    canvas.click(function(e){
        settings.draw = !settings.draw;
    });

    $(window).resize(function() {
        canvas_properties = setUpCanvas("#canvas", {
            globalAlpha: 0.2,
            lineWidth: 16
        });
    });

});