function Matrix(centerX, centerY, squareSize, width, height, ctx) {
	this.squareSize = squareSize;
	this.squares = new Array;

	var squareWidth = Math.round(0.8*width/squareSize);
	squareWidth -= squareWidth % 2;

	for (i=0; i<squareWidth; i++) {

		this.squares[i] = new Array;

		var squareHeight = Math.round(0.8*height/squareSize);
		squareHeight -= squareHeight % 2;

		for (j=0; j<squareHeight; j++) {
			this.squares[i][j] = {
				x:centerX + this.squareSize * (i-squareWidth/2),
				y:centerY - this.squareSize * (squareHeight/2-j)
			};
		}
	}

	this.graph = function() {
		ctx.fillStyle = "#151515";
		for (i=0; i<squareWidth; i++) {
			for (j=0; j<squareHeight; j++) {
				ctx.fillRect(this.squares[i][j].x, this.squares[i][j].y, this.squareSize, this.squareSize);
			}
		}

	}
}

function SnakeList(matrix, ctx) {
	var centerX = matrix.squares.length/2;
	var centerY = matrix.squares[0].length/2;

	this.gameRecord = new Array;
	this.gameRecord[0] = {x:centerX-2, y:centerY};
	this.gameRecord[1] = {x:centerX-1, y:centerY};
	this.gameRecord[2] = {x:centerX  , y:centerY};
	this.gameRecord[3] = {x:centerX+1, y:centerY};
	this.gameRecord[4] = {x:centerX+2, y:centerY};

	this.snakeHeads = new Array;
	this.snakeHeads[0] = 4

	this.snakeColors = new Array;
	this.snakeColors[0] = "rgb(200,200,200)";

	this.direction;
	this.newDirection;

	this.move = function(food) {
		if (typeof this.newDirection != "undefined") {
			this.direction = this.newDirection;

			var last = this.gameRecord.length -1;
			var newPosition = {
				x:this.gameRecord[last].x, 
				y:this.gameRecord[last].y
			};

			if (this.direction == "up") {
				newPosition.y -= 1;
				if (newPosition.y < 0) {
					newPosition.y = matrix.squares[0].length - 1;
				}

			}
			else if (this.direction == "down") {
				newPosition.y += 1;
				if (newPosition.y > matrix.squares[0].length - 1) {

					newPosition.y = 0;
				}
			}
			else if (this.direction == "left") {
				newPosition.x -= 1;
				if (newPosition.x < 0) {
					newPosition.x = matrix.squares.length - 1;
				}
			}
			else if (this.direction == "right") {
				newPosition.x += 1;
				if (newPosition.x > matrix.squares.length - 1) {
					newPosition.x = 0;
				}
			}

			this.gameRecord.push(newPosition);

			for (i=0; i<this.snakeHeads.length; i++) {
				this.snakeHeads[i]+=1;
			}
			
			if (food.x == newPosition.x && food.y == newPosition.y) {
				this.snakeHeads.push(0);
;				this.snakeColors.push(food.color);
				food.getRandomPosition();
				food.getRandomColor();

			}

		}

	}

	this.collision = function() {
		var snakePos = this.snakeHeads[0];
		for (i=1; i<this.snakeHeads.length; i++) {
			var otherSnakePos = this.snakeHeads[i];
			for (j=0; j<5; j++) {
				var intersect = (this.gameRecord[snakePos-j].x == this.gameRecord[otherSnakePos].x);
				intersect = intersect && (this.gameRecord[snakePos-j].y == this.gameRecord[otherSnakePos].y);
				if (intersect) {
					return true;
				}
			}
			for (j=0; j<5; j++) {
				if (otherSnakePos-j>=0) {
					var intersect = (this.gameRecord[snakePos].x == this.gameRecord[otherSnakePos-j].x);
					intersect = intersect && (this.gameRecord[snakePos].y == this.gameRecord[otherSnakePos-j].y);
					if (intersect) {
						return true;
					}
				}
			}
		}
		return false;
	}

	this.graph = function () {
		for (i=this.snakeHeads.length-1; i>=0; i--) {
			ctx.fillStyle = this.snakeColors[i];
			for (j=0; j<5; j++) {
				var k = this.snakeHeads[i]-j
				if (k>=0) {
					var f = this.gameRecord[k].x;
					var c = this.gameRecord[k].y;
					var x = matrix.squares[f][c].x;
					var y = matrix.squares[f][c].y;
					ctx.fillRect(x, y, matrix.squareSize, matrix.squareSize);
				}
			}

		}
	}

}

function Food(matrix, snakeList, ctx) {
	this.color;
	this.x;
	this.y;


	this.getRandomColor = function() {
		var c = [ equiprobable(4,16)*16, equiprobable(4,16)*16, equiprobable(4,16)*16];
		this.color = "rgb(" + c[0] + "," + c[1] + "," + c[2] + ")";
	}

	this.getRandomColor();

	this.graph = function() {
		if (typeof this.x != "undefined") {
			ctx.fillStyle = this.color;
			var posX = matrix.squares[this.x][this.y].x;
			var posY = matrix.squares[this.x][this.y].y;
			ctx.fillRect(posX, posY, matrix.squareSize, matrix.squareSize);
		}

	}

	this.getRandomPosition = function() {
		var validPlace = false
		while (!validPlace) {
			this.x = equiprobable(0, matrix.squares.length-1);
			this.y = equiprobable(0, matrix.squares[0].length-1);

			var i = snakeList.snakeHeads[0]
			var snakePosX = snakeList.gameRecord[i].x;
			var snakePosY = snakeList.gameRecord[i].y;

			if (this.x!= snakePosX && this.y!=snakePosY) {
				validPlace=true;
			}
		}

	}

}

function Info(matrix, snakeList, ctx, width, height) {
	var last = matrix.squares[0].length-1;
	this.x = matrix.squares[0][last].x ;
	this.y = matrix.squares[0][last].y + 2*matrix.squareSize;
	this.score = 0;
	this.text = "Restart : r     Puntaje : " + (Math.round(this.score) + snakeList.snakeHeads.length*100-100);
	this.graph = function () {
		ctx.fillStyle = "#DDDDDD";
		ctx.font= matrix.squareSize + 'px Courier New'
		ctx.clearRect(this.x-matrix.squareSize, this.y-matrix.squareSize, width, height);
		ctx.fillText(this.text, this.x, this.y);
		this.text = "Restart : r     Puntaje : " + (Math.round(this.score) + snakeList.snakeHeads.length*100-100);
	}
}

function uniform(a,b) {
	return Math.random()*(b-a) + a;
}

function equiprobable(a,b) {
	return Math.floor(Math.random()*(1+b-a) + a);
}

