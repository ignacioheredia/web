// ignacio.nh (at) gmail.com

function main () {
	var width = document.documentElement.clientWidth;
	var height = document.documentElement.clientHeight;

	var backgroundLayer = document.getElementById('background').getContext("2d");
	var snakeLayer = document.getElementById('snake').getContext("2d");

	document.getElementById('background').width = width;
	document.getElementById('background').height = height;

	document.getElementById('snake').width = width;
	document.getElementById('snake').height = height;

	var squareSize = Math.round(15 * width * height / 1535520)
	var centerX = Math.round(width/2-squareSize/2);
	var centerY = Math.round(height/2-squareSize/2);

	var matrix = new Matrix(centerX, centerY, squareSize, width, height, backgroundLayer);
	matrix.graph();

	var snakeList = new SnakeList(matrix, snakeLayer);
	var food = new Food(matrix, snakeList, snakeLayer);
	var info = new Info(matrix, snakeList, backgroundLayer, width, height);

	var FPS=20;
	if (typeof handle == "undefined") {
		handle = window.setInterval(animation, 1000/FPS);
	}

	function animation() {
		snakeLayer.clearRect(0, 0, width, height);
		if ( !snakeList.collision() ) {
			snakeList.move(food);
			if (typeof snakeList.direction != "undefined") {
				info.score += (snakeList.snakeHeads.length-1)/50;
			}
		}
		snakeList.graph();
		food.graph();
		info.graph();
	}

	addEventListener( "keydown", keyDown, true );

	function keyDown(event) {
		var key = event.keyCode

		if (key == 37 && snakeList.direction!="right" && typeof snakeList.direction != "undefined") {
			// Izquierda
			snakeList.newDirection = "left";
		}
		else if (key == 38 && snakeList.direction!="down") {
			// Arriba 
			snakeList.newDirection = "up";
		}
		else if (key == 39 && snakeList.direction!="left") {
			// Derecha
			snakeList.newDirection = "right";
		}
		else if (key == 40 && snakeList.direction!="up") {
			// Abajo
			snakeList.newDirection = "down";
		}
		else if (key == 82) {
			// Restart
			snakeList = new SnakeList(matrix, snakeLayer);
			food = new Food(matrix, snakeList, snakeLayer);
			info = new Info(matrix, snakeList, backgroundLayer, width, height);
		}

		if (typeof food.x == "undefined" && typeof snakeList.newDirection != "undefined") {
			food.getRandomPosition();
			food.graph();
		}
	}

}

function resize() {
	// TO DO 
}


main();

