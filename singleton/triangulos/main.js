var width;
var height;

var FPS=24;
var ctx = document.getElementById('canvas').getContext('2d');

var triangulos = new Array();
var base ;
var altura;
var listaColores = [[95,158,160],[46,139,87],[165,42,42],[147,112,219]]; // celeste, verde, rojo, violeta
var color = listaColores[Math.floor(Math.random()*listaColores.length)];
var trianguloActual = 5;

function resize() {
	width = document.documentElement.clientWidth;
	height = document.documentElement.clientHeight;
	document.getElementById("canvas").width = width;
	document.getElementById("canvas").height = height;

	inicializar();
}

function inicializar() {
	var altura = height;
	var base = Math.round(altura * 2 /Math.sqrt(3));

	var x0 = Math.round((width-base)/2);
	var x1 = Math.round((width+base)/2);
	var x2 = Math.round(width/2);
	var y0 = Math.round((height+altura)/2);
	var y1 = Math.round((height+altura)/2);
	var y2 = Math.round((height-altura)/2);

	triangulos[0] = new Triangulo(x0,y0,x1,y1,x2,y2,colorAleatorio(),1);
	triangulos[1] = new Triangulo(x0-base,y0,x1-base,y1,x2-base,y2,colorAleatorio(),1);
	triangulos[2] = new Triangulo(x0+base,y0,x1+base,y1,x2+base,y2,colorAleatorio(),1);
	triangulos[3] = new Triangulo(x2-base,y2,x2,y2,x0,y0,colorAleatorio(),1);
	triangulos[4] = new Triangulo(x2,y2,x2+base,y2,x1,y1,colorAleatorio(),1);
	triangulos[0].graficar();
	triangulos[1].graficar();
	triangulos[2].graficar();
	triangulos[3].graficar();
	triangulos[4].graficar();

/*
	triangulos[0] = new Triangulo(0,height,width,height,Math.round(width/2),0,colorAleatorio(),1);
	triangulos[1] = new Triangulo(Math.round(-width/2),0,Math.round(width/2),0,0,height,colorAleatorio(),1);
	triangulos[2] = new Triangulo(Math.round(width/2),0,Math.round(width*3/2),0,width,height,colorAleatorio(),1);
	triangulos[0].graficar();
	triangulos[1].graficar();
	triangulos[2].graficar();
*/
}

function mouseMove(event) {
	var x=event.clientX;
	var y=event.clientY;
	for (i=0; i<triangulos.length; i++) {
		if (triangulos[i].mouseOver(x,y)) {
			if (trianguloActual == -1) {
				trianguloActual=i;
			}
			else if (trianguloActual!=i) {
				trianguloActual= -1;
				var tr = triangulos[i];
				separar(i);

			}
			break;
		}
	}
}

function angulo(x,y,b) {
	return Math.abs(Math.atan(x/y));
}

function dentroDelCuadrado(x0,x1,y0,y2,x,y) {
	if (y0 < y2) {
		return (x0 < x && x < x1 && y0 < y && y < y2);
	}
	else {
		return (x0 < x && x < x1 && y2 < y && y < y0);
	}
}

function colorAleatorio() {
	var offsetColor = Math.round(Math.random()*80-40);
	return [color[0]+offsetColor,color[1]+offsetColor,color[2]+offsetColor];
}

function separar(i) {
	var x0 = triangulos[i].x0;
	var y0 = triangulos[i].y0;
	var x1 = triangulos[i].x1;
	var y1 = triangulos[i].y1;
	var x2 = triangulos[i].x2;
	var y2 =  triangulos[i].y2;
	var x3 = Math.round(x0+(x1-x0)/4);
	var x4 = x2;
	var x5 = Math.round(x1-(x1-x0)/4);
	var y3 = Math.round(y0-(y0-y2)/2);
	var y4 = y0;
	var y5 = y3;

	triangulos[i].x1 = x4;
	triangulos[i].y1 = y4;
	triangulos[i].x2 = x3;
	triangulos[i].y2 = y3;
	triangulos[i].color = colorAleatorio();
	triangulos[i].profundidad += 1;
//	var t0 = new Triangulo(t.x0, t.y0, x4, y4, x3, y3, colorAleatorio(), t.profundidad+1);
	var t1 = new Triangulo(x4, y4, x1, y1, x5, y5, colorAleatorio(), triangulos[i].profundidad);
	var t2 = new Triangulo(x3, y3, x5, y5, x2, y2, colorAleatorio(), triangulos[i].profundidad);
	var t3 = new Triangulo(x3, y3, x5, y5, x4, y4, colorAleatorio(), triangulos[i].profundidad);

//	t0.graficar();
	triangulos[i].graficar();
	t1.graficar();
	t2.graficar();
	t3.graficar();

//	triangulos.push(t0);
	triangulos.push(t1);
	triangulos.push(t2);
	triangulos.push(t3);
}

function Triangulo(x0,y0,x1,y1,x2,y2,color,p) {
	this.x0 = x0;
	this.y0 = y0;
	this.x1 = x1;
	this.y1 = y1;
	this.x2 = x2;
	this.y2 = y2;
	this.color = color;
	this.profundidad = p;

	this.mouseOver = mouseOver;
	function mouseOver(x,y) {
		if (dentroDelCuadrado(this.x0,this.x1,this.y0,this.y2,x,y)) {
			if (angulo((this.y0-y),(x-this.x0),true)<(Math.PI/3) && angulo((this.y0-y),(this.x1-x),false)<(Math.PI/3) ) {
				return true;
			}
		}
		return false;
	}

	this.graficar = graficar;
	function graficar() {
		ctx.fillStyle = "rgb("+this.color[0]+","+this.color[1]+","+this.color[2]+")";
		ctx.beginPath();
		ctx.moveTo(this.x0,this.y0);
		ctx.lineTo(this.x1,this.y1);
		ctx.lineTo(this.x2,this.y2);
		ctx.fill();
	}
}

