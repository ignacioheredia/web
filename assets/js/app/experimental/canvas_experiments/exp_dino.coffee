CanvasExperiments ||= {}

class CanvasExperiments.Dino extends CanvasExperiment

  id: 'dino-container'
  className: 'experiment-container'

  experimentResize: ->
    @startExperiment()

  startExperiment: ->