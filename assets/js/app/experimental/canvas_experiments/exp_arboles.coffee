CanvasExperiments ||= {}

class CanvasExperiments.Arboles extends CanvasExperiment

  id: 'arboles-container'
  className: 'experiment-container'

  startExperiment: =>
    @maxDepth = 13
    @firstRectangle = @startingRectangle()
    @currentLayer = [@firstRectangle]
    @nextLayer = []
    @currentDepth = 0
    setTimeout(@draw, 1000/@fps)

  experimentResize: ->
    clearTimeout(@timeout)
    @ctx.clearRect(0, 0, @width, @height)
    @startExperiment()

  experimentRefresh: ->
    @experimentResize()

  draw: =>
    times = Math.pow(2, @currentDepth)/4
    for [0...times]
      break if @currentLayer.length == 0
      rectangle = @currentLayer.pop()
      rectangle.draw()
      if rectangle.depth < @maxDepth
        rectangle.seedNext()
        @nextLayer.push(rectangle.nextLeft())
        @nextLayer.push(rectangle.nextRight())

    if @currentLayer.length == 0
      @currentLayer = chance.shuffle(@nextLayer)
      @nextLayer = []
      @currentDepth += 1

    if @currentLayer.length > 0
      @timeout = setTimeout(@draw, 1000/@fps)

  startingRectangle: ->
    referenceSize = Math.min(@width, @height)
    rectWidth = chance.natural(min: referenceSize/6, max: referenceSize/5)
    rectHeight = chance.natural(min: referenceSize/5, max: referenceSize/4)
    new Rectangle(
      ctx: @ctx
      bottomLeft:
        x: (@width-rectWidth)/2
        y: @height - 10
      bottomRight:
        x: (@width+rectWidth)/2
        y: @height - 10
      topRight:
        x: (@width+rectWidth)/2
        y: @height - 10 - rectHeight
      topLeft:
        x: (@width-rectWidth)/2
        y: @height - 10 - rectHeight
      angle: 0
      depth: 0
    )


class Rectangle

  constructor: (@options) ->
    @ctx = @options.ctx
    @bottomLeft = @options.bottomLeft
    @bottomRight = @options.bottomRight
    @topRight = @options.topRight
    @topLeft = @options.topLeft
    @color = @options.color
    @depth = @options.depth
    @angle = @options.angle

  draw: ->
    @ctx.fillStyle = @fillStyle()
    @ctx.beginPath()
    @ctx.moveTo(@bottomLeft.x, @bottomLeft.y)

    @ctx.lineTo(@bottomRight.x, @bottomRight.y)
    @ctx.lineTo(@topRight.x, @topRight.y)
    @ctx.lineTo(@topLeft.x, @topLeft.y)
    @ctx.lineTo(@bottomLeft.x, @bottomLeft.y)

    @ctx.fill()

  fillStyle: ->
    if @depth < 3
      color = [100, 50, 10] # brown
      variation = chance.integer(min: -10, max: 10)
    else
      color = [0, 100, 0] # green
      variation = chance.integer(min: -40, max: 40)
    color[0] += variation
    color[1] += variation
    color[2] += variation
    "rgb(#{color[0]}, #{color[1]}, #{color[2]})"

  baseWidth: ->
    x = @bottomLeft.x - @bottomRight.x
    y = @bottomLeft.y - @bottomRight.y
    Math.sqrt(x * x + y * y);

  seedNext: ->
    @newAngle = (45 + chance.integer(min: -5, max: 5)) % 360 # from topLeft corner
    @newWidth = @_cos(@newAngle) * @baseWidth()
    @newHeight = @newWidth * chance.floating(min: 1, max: 1.2)
    @newAngle = @newAngle + @angle

  nextLeft: ->
    bottomLeft =
      x: @topLeft.x
      y: @topLeft.y
    bottomRight =
      x: bottomLeft.x + @newWidth * @_cos(@newAngle)
      y: bottomLeft.y - @newWidth * @_sin(@newAngle)
    topRight =
      x: bottomRight.x + @newHeight * @_cos(@newAngle + 90)
      y: bottomRight.y - @newHeight * @_sin(@newAngle + 90)
    topLeft =
      x: bottomLeft.x + @newHeight * @_cos(@newAngle + 90)
      y: bottomLeft.y - @newHeight * @_sin(@newAngle + 90)

    @newRectangle(bottomLeft, bottomRight, topRight, topLeft, @newAngle)

  nextRight: ->
    bottomLeft =
      x: @topLeft.x + @newWidth * @_cos(@newAngle)
      y: @topLeft.y - @newWidth * @_sin(@newAngle)
    bottomRight =
      x: @topRight.x
      y: @topRight.y
    topRight =
      x: @topRight.x + @newHeight * @_cos(@newAngle)
      y: @topRight.y - @newHeight * @_sin(@newAngle)
    topLeft =
      x: bottomLeft.x + @newHeight * @_cos(@newAngle)
      y: bottomLeft.y - @newHeight * @_sin(@newAngle)

    @newRectangle(bottomLeft, bottomRight, topRight, topLeft, @newAngle - 90)

  newRectangle: (bottomLeft, bottomRight, topRight, topLeft, angle) ->
    new Rectangle(
      ctx: @ctx
      bottomLeft: bottomLeft
      bottomRight: bottomRight
      topRight: topRight
      topLeft: topLeft
      angle: angle
      depth: @depth + 1
    )

  _cos: (angle) ->
    Math.cos(angle*Math.PI/180)

  _sin: (angle) ->
    Math.sin(angle*Math.PI/180)