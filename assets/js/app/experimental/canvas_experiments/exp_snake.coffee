CanvasExperiments ||= {}

class CanvasExperiments.Snake extends CanvasExperiment

  id: 'snake-container'
  className: 'experiment-container'

  experimentResize: ->
    @startExperiment()

  startExperiment: ->