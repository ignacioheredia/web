CanvasExperiments ||= {}

class CanvasExperiments.Random5 extends CanvasExperiment

  id: 'random5-container'
  className: 'experiment-container'

  experimentResize: ->
    @startExperiment()

  startExperiment: ->