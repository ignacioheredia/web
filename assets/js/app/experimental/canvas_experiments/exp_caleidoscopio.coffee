CanvasExperiments ||= {}

class CanvasExperiments.Caleidoscopio extends CanvasExperiment

  id: 'caleidoscopio-container'
  className: 'experiment-container'

  experimentResize: ->
    @startExperiment()

  startExperiment: ->