class ProcessingExperiment extends TemplateView

  className: 'experiment-container'

  initialize: (@options) ->
    @template = @loadTemplate('/templates/experimental/processing_experiment.jst')
    $(window).on('resize', @onResize)
    console.log(@options)

  render: ->
    @$el.html(@template())

    @$canvas = @$el.find('canvas')
    @canvas = @$canvas[0]

    @$canvas.fadeIn(500, @onResize)

  startExperiment: =>
    @$canvas.attr('data-processing-sources', @options.pdePath)
    Processing.reload()

  onResize: =>
    @width = @$canvas.width()
    @height = @$canvas.height()
    window.canvasWidth = @width
    window.canvasHeight = @height