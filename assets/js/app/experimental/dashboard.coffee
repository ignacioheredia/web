class ExperimentalDashboard extends TemplateView

  initialize: ->
    @template = @loadTemplate('/templates/experimental/dashboard.jst')
    @experimentTemplate = @loadTemplate('/templates/experimental/dashboard_experiment.jst')
    @experiments = @loadJSON('/config/experiments.json') unless @experiments

  dashboard: (section) ->
    @$el = $(@template())
    @el = @$el[0]
    if @[section]
      @[section]()

    exp = @findExperiment(section)
    if exp
      @renderExperiment(exp)
    else
      @renderPreviews()

  renderPreviews: ->
    return unless @experiments.length > 0
    for exp in @experiments
      template = @experimentTemplate(experiment: exp)
      exp.$el = $(template)
      exp.el = exp.$el[0]
      exp.$el.find('img').error(@imageError)
      @$el.find('#experiment-previews').append(exp.$el)
      exp.$el.css('visibility', 'hidden')
    index = 0
    timeout = setInterval(=>
      $el = @experiments[index].$el
      $el.css('visibility', 'visible').hide().fadeIn()
      index += 1
      clearInterval(timeout) if index == @experiments.length
    , 50)

  findExperiment: (code) ->
    _.find(@experiments, (exp) -> exp.code == code)

  renderExperiment: (exp) ->
    if exp.processingExperiment
      exp.app ||= new ProcessingExperiment(pdePath: exp.pdePath)
    else
      exp.app ||= new CanvasExperiments[exp.name](code: exp.code)
    exp.app.render()
    @$el.find('#experiment-previews').fadeOut(300, =>
      @$el.find('#experiment-previews').remove()
      @$el.append(exp.app.el)
      setTimeout(exp.app.startExperiment, 700)
    )

  imageError: (evt) ->
    $(evt.currentTarget).attr('src', '/assets/img/exp_not_found.png')