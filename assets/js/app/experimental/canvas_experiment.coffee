CanvasExperiments = {}

class CanvasExperiment extends TemplateView

  events:
    'click #experiment-refresh': 'onRefresh'
    'click #experiment-maximize': 'onMaximize'
    'click #experiment-restore': 'onRestore'
    'click #experiment-code': 'onShowCode'
    'click #experiment-image': 'onExportImage'

  initialize: (@options) ->
    @template = @loadTemplate('/templates/experimental/canvas_experiment.jst')
    $(window).on('resize', @onResize)
    @fps = 24

  render: ->
    @$el.html(@template(code: @options.code))

    @canvas = @$el.find('canvas')
    @ctx = @canvas[0].getContext('2d')

    @$el.find('canvas').fadeIn(500, @updateSize)

  updateSize: =>
    @width = @canvas.width()
    @height = @canvas.height()
    @canvas.attr('width', @width)
    @canvas.attr('height', @height)

  onResize: =>
    @updateSize()
    @experimentResize()

  onRefresh: =>
    @experimentRefresh()

  onMaximize: =>
    @$el.find('.size-controller').toggle()
    @$el.addClass('maximized')
    setTimeout(@onResize, 100)

  onRestore: =>
    @$el.find('.size-controller').toggle()
    @$el.removeClass('maximized')
    setTimeout(@onResize, 100)

  onShowCode: =>
    # TODO

  onExportImage: =>
    # TODO