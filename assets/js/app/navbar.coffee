class NavBar extends TemplateView

  id: 'nav-parent'

  initialize: ->
    @template ||= @loadTemplate('/templates/navbar.jst')

  render: ->
    @$el.html(@template())

  setActive: (id) ->
    @$el.find('.active').removeClass('active')
    @$el.find("##{id}-navbtn").addClass('active')