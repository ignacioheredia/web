class AboutDashboard extends TemplateView

  id: 'about-container'
  templatePath: '/templates/about.jst'

  initialize: ->
    @template = @loadTemplate(@templatePath)

  render: ->
    @$el.html(@template())
    @$el.fadeIn(400)