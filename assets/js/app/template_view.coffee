class TemplateView extends Backbone.View

  loadTemplate: (url) =>
    template = null
    $.ajax(
      url: url
      data: {}
      success: (response) ->
        template = _.template response
      async: false
    )
    template

  loadJSON: (url) ->
    json = null
    $.ajax(
      url: url
      dataType: 'json'
      async: false
      success: (response) ->
        json = response
    )
    json