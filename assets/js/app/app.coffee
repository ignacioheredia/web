class App extends Backbone.Router

  routes:
    '': 'experimental'
    'about(/)': 'about'
    'blog(/)': 'blog'
    'blog(/:section)': 'blog'
    'experimental(/)': 'experimental'
    'experimental(/:section)': 'experimental'

  initialize: ->
    @$el = $('body')
    @el = @$el[0]

    @navBar = new NavBar()
    @experimentalDashboard = new ExperimentalDashboard()
    @blogDashboard = new BlogDashboard()
    @aboutDashboard = new AboutDashboard()

    @render()

  render: ->
    @navBar.render()
    @$el.prepend(@navBar.el)

  errorPage: =>
    # TODO

  about: =>
    @navBar.setActive('about')
    @fadeAll =>
      @aboutDashboard.render()
      @$el.find('#container').html(@aboutDashboard.el)

  blog: =>
    @navBar.setActive('blog')
    @fadeAll()
  # TODO

  experimental: (section) =>
    @navBar.setActive('experimental')
    @fadeAll =>
      @experimentalDashboard.dashboard(section)
      @$el.find('#container').html(@experimentalDashboard.el)

  fadeAll: (callback) ->
    @experimentalDashboard.$el.fadeOut(400)
    @blogDashboard.$el.fadeOut(400)
    @aboutDashboard.$el.fadeOut(400)
    setTimeout(callback, 500)

$ ->
  new App
  Backbone.history.stop()
  Backbone.history.start()