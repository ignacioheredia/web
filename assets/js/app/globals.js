function cosine(a) {
	// a en grados
	return Math.cos(a*Math.PI/180);
}

function sine(a) {
	// a en grados
	return Math.sin(a*Math.PI/180);
}

function sign(n) {
    if (n>0) {return 1;}
    else if (n<0) {return -1;}
    return 0;
}

function distance(obj1, obj2) {
    return Math.sqrt( (obj1.x-obj2.x)*(obj1.x-obj2.x) + (obj1.y-obj2.y)*(obj1.y-obj2.y) );
}

function randomVariable() {
	return Math.random();
}

randomVariable.Uniform = function(a,b) {
	// Variable aleatoria con distribucion uniforme(a,b)
	return Math.random()*(b-a) + a;
};

randomVariable.Equiprobable = function(a,b) {
	// Variable aleatoria con distribucion equiprobable(a,b)
	return Math.floor(Math.random()*(1+b-a) + a);
};

function Color(params) {
    params = params || {};
    var min = params.min || 0;
    var max = params.max || 255;
	this.vector = [];
	for (var i=0; i<3; i++) {
		this.vector[i] = randomVariable.Equiprobable(min, max);
	}
}

Color.prototype.toString = function(outputType) {
	outputType = outputType || 'hex';
	var exitString = "#";

	switch(outputType) {
		case 'hex': 
			for (var i=0; i<3; i++) {
				var tempString = this.vector[i].toString(16);
				if (tempString.length == 1) { 
					tempString = "0" + tempString;
				}
				exitString += tempString;
			}
            break;
		case 'rgb':
			break;
		case 'rgba':
			break
	}

	return exitString;
};

Color.prototype.mutate = function(strength) {
    strength = strength || 1;

    var i = randomVariable.Equiprobable(0,2);
    this.vector[i] += randomVariable.Equiprobable(-strength, strength);
    this.vector[i] = Math.min(this.vector[i], 255);
    this.vector[i] = Math.max(this.vector[i], 0);
};

function setUpCanvas(id, params) {
    params = params || {};

    var canvas = $(id);

    var properties = {
        width: canvas.width(),
        height: canvas.height(),
        diagonal: Math.round(Math.sqrt( canvas.width()*canvas.width() + canvas.height()*canvas.height())),
        ctx: canvas[0].getContext("2d")
    };

    canvas[0].width = properties.width;
    canvas[0].height = properties.height;

    properties.ctx.globalAlpha = params.globalAlpha || 1;
    properties.ctx.lineWidth = params.lineWidth || 1;

	return properties;
}
